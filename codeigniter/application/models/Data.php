<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Model{
    public function kategoribyproduk(){
        $data = $this->db->query("SELECT DISTINCT id_kategori, nama_kategori FROM produk WHERE sts='1' AND thumbnail != ''");
        $this->db->close();
        return $data->result();
    }
}
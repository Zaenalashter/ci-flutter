<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Data');
	}
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function pages($judul=''){
		if($judul=='kategoribyproduk'){
			$sData = array();
			$data= $this->Data->kategoribyproduk();
			foreach($data as $d){
				$arr_row = array();
				$arr_row['id_kategori'] = $d->id_kategori;
				$arr_row['nama_kategori'] = $d->nama_kategori;
				$sData[] = $arr_row;
			}
			header('Content-Type: application/json');
			echo json_encode($sData, JSON_PRETTY_PRINT);
		}
	}
}

import 'package:flutter/material.dart';
import 'package:toko_online/constans.dart';
import 'package:toko_online/users/akunpage.dart';
import 'package:toko_online/users/beranda.dart';
import 'package:toko_online/users/favoritepage.dart';
import 'package:toko_online/users/keranjangpage.dart';
import 'package:toko_online/users/transaksipage.dart';

class LandingPage extends StatefulWidget {
  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new Beranda(),
    new FavoritePage(),
    new KeranjangPage(),
    new TransaksiPage(),
    new AkunPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _container[_bottomNavCurrentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Palette.bg1,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() {
            _bottomNavCurrentIndex = index;
          });
        },
        currentIndex: _bottomNavCurrentIndex,
        items: [
          BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.home,
              color: Palette.bg1
            ),
            icon: new Icon(
              Icons.home,
              color: Colors.grey
            ),
            label: 'Beranda',
          ),
          BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.favorite,
              color: Palette.bg1
            ),
            icon: new Icon(
              Icons.favorite,
              color: Colors.grey
            ),
            label: 'Favorite',
          ),
          BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.shopping_cart,
              color: Palette.bg1
            ),
            icon: new Icon(
              Icons.shopping_cart,
              color: Colors.grey
            ),
            label: 'Keranjang',
          ),
          BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.swap_horiz_sharp,
              color: Palette.bg1
            ),
            icon: new Icon(
              Icons.swap_horiz_sharp,
              color: Colors.grey
            ),
            label: 'Transaksi',
          ),
          BottomNavigationBarItem(
            activeIcon: new Icon(
              Icons.account_circle,
              color: Palette.bg1
            ),
            icon: new Icon(
              Icons.account_circle,
              color: Colors.grey
            ),
            label: 'Profile',
          ),
          
        ],
      ),
    );
  }
}

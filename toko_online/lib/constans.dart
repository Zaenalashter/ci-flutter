import 'package:flutter/material.dart';

class Palette{
  //HP
  // static String sUrl = "http://192.168.11.141/codeigniter"; 
  //EMULATOR
  static String sUrl = "http://10.0.2.2/codeigniter";
  static Color bg1 = Color.fromRGBO(0, 0, 102, 1);
  static Color bg2 = Color.fromRGBO(0, 0, 110, 1);
  static Color orange = Color(0xfff7892b);


  static List<Color> colors = <Color>[
    Color.fromARGB(255, 255, 0, 0),
    Color.fromARGB(255, 128, 0, 0),
    Color.fromARGB(255, 0, 178, 0),
    Color.fromARGB(255, 0, 0, 255),
    Color.fromARGB(255, 127, 0, 255),
    Color.fromARGB(255, 255, 0, 255),
  ];

}
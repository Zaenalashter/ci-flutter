class Kategori{
  final int id;
  final String kategori;

  Kategori({required this.id, required this.kategori});

  factory Kategori.fromJson(Map<String, dynamic> json){
    return Kategori(
      id: json['id_kategori'] as int,
      kategori: json['nama_kategori'] as String,
    );
  }
}